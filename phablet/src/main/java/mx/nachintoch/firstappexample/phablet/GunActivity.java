package mx.nachintoch.firstappexample.phablet;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import mx.nachintoch.firstappexample.DrawTimer;
import mx.nachintoch.firstappexample.Pedometer;
import mx.nachintoch.firstappexample.SoundPlayer;

/**
 * Phone & Tablet activity with the fire button and the gun image to display.
 * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch" Castillo</a>
 * @version 2.0, march 2021
 * @since Cowboy duel 1.0, february 2019
 */
public class GunActivity extends AppCompatActivity {

    // class attributes

    /**
     * If this device runs Android Kitkat or newer, then this will hold a reference to the Sensor
     * Manager of the device, so the step counter sensor can be used. If the device runs an older
     * version of Android or does not support the step counter sensor, this will be null.
     * @since Gun Activity 1.0, february 2019
     */
    @Nullable
    private SensorManager sensorManager;

    /**
     * Reference to the retrieved Step Sensor; if available.
     * @since Gun Activity 1.0, february 2019
     */
    @Nullable
    private Sensor stepDetectorSensor;


    /**
     * Reference to the application's pedometer; if available by the system sensors.
     * @since Gun Activity 1.0, february 2019
     */
    @Nullable
    private Pedometer pedometer;

    /**
     * Starts a count in a separate thread.
     * @since Gun Activity 1.0, february 2019
     */
    @Nullable
    private DrawTimer asyncCounter;

    /**
     * Executes parallel tasks, managing a single background thread.
     * @since Gun Activity 2.0, march 2021
     */
    private ExecutorService singleThreadProducer;

    /**
     * Reference to the gun view.
     * @since Gun Activity 1.0, february 2019
     */
    private ImageView gunView;

    /**
     * Reference to the startButton
     * @since Gun Activity 1.0, february 2019
     */
    private Button startButton;

    // methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(mx.nachintoch.firstappexample.phablet.R.layout.activity_gun);
        gunView = findViewById(R.id.gun_iv);
        startButton = findViewById(R.id.start_button);
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        if(sensorManager != null) {
            stepDetectorSensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);
        }
        if(stepDetectorSensor == null) {
            sensorManager = null;
        } else {
            pedometer = new Pedometer(sensorManager, gunView);
        }
    }//onCreate

    @Override
    protected void onPause() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        killCounter();
        super.onPause();
    }//onPause

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if(hasFocus) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER_LANDSCAPE);
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }//if has focus
    }//onWindowFocusChanged

    @Override
    protected void onResume() {
        super.onResume();
        init();
    }//onResume

    /**
     * In Android versions prior to KitKat (API level 19), it starts a counter from 3 to 0, so the
     * gun is drawn. In newer versions of Android, this method starts the pedometer to count 3 steps
     * before drawing the gun.
     * @param startButton - Reference to the startButton.
     * @since Gun Activity 1.0, february 2019
     */
    public void finalCountdown(View startButton) {
        startButton.setVisibility(View.INVISIBLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        checkStepSensor();
    }//finalCountdown

    /**
     * Initializes the AsyncTask for this class and counts 3 seconds before showing the gun.
     * @since GunActivity 1.0, february 2019
     */
    private void startTimer() {
        if(singleThreadProducer == null) {
            singleThreadProducer = Executors.newSingleThreadExecutor();
        } else if(asyncCounter != null) {
            asyncCounter.cancel();
        }
        asyncCounter = new DrawTimer(gunView, Pedometer.STEPS_TO_COUNT);
        singleThreadProducer.execute(asyncCounter);
        Log.d(GunActivity.class.getSimpleName(), "Using timer");
    }//startTimer

    /**
     * Checks if the devices has a Step Sensor. If it does, it starts the step counting. If doesn't,
     * if fallbacks to a timer.
     * @since Gun Activity 1.0, february 2019
     */
    private void checkStepSensor() {
        if(sensorManager == null) {
            startTimer();
            return;
        }//if no pedometer, then count time
        sensorManager.registerListener(pedometer, stepDetectorSensor,
                SensorManager.SENSOR_DELAY_NORMAL);
        Log.d(GunActivity.class.getSimpleName(), "Using pedometer");
    }//checkStepSensor

    /**
     * Plays the gun shot and resets the app.
     * @param gun - Reference to the gun view.
     * @since Gun Activity 1.0, february 2019
     */
    public void fire(View gun) {
        SoundPlayer.enqueueWork(this, new Intent(SoundPlayer.ACTION_FIRE));
        gun.setVisibility(View.INVISIBLE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                init();
            }//run
        }, 3000);
    }//fire

    /**
     * Sets the application as in it initial state: shows the start button and hides the gun view.
     * @since Gun Activity 1.0, february 2019
     */
    private void init() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            if(!checkActivityRecognitionPermission()) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACTIVITY_RECOGNITION},0);
            }
        }
        startButton.setVisibility(View.VISIBLE);
        gunView.setVisibility(View.INVISIBLE);
    }//init

    /**
     * Stops the Step count or the Ti me counter, depending on the availability.
     * @since Gun Activity 1.0, february 2019
     */
    private void killCounter() {
        if(sensorManager != null) {
            sensorManager.unregisterListener(pedometer);
        } else if(asyncCounter != null) {
            asyncCounter.cancel();
        }
    }//killCounter

    /**
     * Check whether the user has granted or not the ACTIVITY_RECOGNITION permission.
     * @return boolean - Whether the permission has ben granted (true) or not
     */
    @TargetApi(29)
    private boolean checkActivityRecognitionPermission() {
        return ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACTIVITY_RECOGNITION) == PackageManager.PERMISSION_GRANTED;
    }//checkActivityRecognitionPermission

}//GunActivity
