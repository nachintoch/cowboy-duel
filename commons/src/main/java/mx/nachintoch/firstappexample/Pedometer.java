package mx.nachintoch.firstappexample;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import androidx.annotation.NonNull;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;

/**
 * Uses the Sensor.TYPE_STEP_DETECTOR to count steps; in other words, this class is a Pedometer.
 * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch" Castillo</a>
 * @version 1.1, march 2021
 * @since Cowboy Duel 1.0, february 2019
 */
public final class Pedometer implements SensorEventListener {
    /**
     * Holds how many seconds are going to be counted.
     * @since AsyncCounter 1.0, february 2019
     */
    public static final byte STEPS_TO_COUNT = 3;

    // class attributes 

    /**
     * Counts the step given by the user.
     * @since Pedometer 1.0, february 2019
     */
    private byte steps;

    /**
     * Reference to the system sensor manager, so this listener can be unregistered once the three
     * steps have been reached. This reduces power & processor usage.
     * @since Pedometer 1.0, february 2019
     */
    @NonNull
    private final SensorManager SENSOR_MANAGER;

    /**
     * Reference to the view with the gun to show when the three steps are reached.
     * @since Pedometer 1.0, february 2019
     */
    @NonNull
    private final ImageView GUN_VIEW;

    // constructor

    /**
     * Sets the references for the device Sensor Manager and the application's gun view.
     * @param sensorManager - Reference to the system manager.
     * @param gunView - Reference to the gun view.
     * @since Pedometer 1.0, february 2019
     */
    public Pedometer(@NonNull SensorManager sensorManager, @NonNull ImageView gunView) {
        SENSOR_MANAGER = sensorManager;
        GUN_VIEW = gunView;
    }//constructor

    // methods
    
    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        postProgress();
        steps++;
        if(steps >= STEPS_TO_COUNT) {
            SENSOR_MANAGER.unregisterListener(this);
            GUN_VIEW.setVisibility(View.VISIBLE);
            steps = 0;
        }//if the three steps are reached
    }//onSensorChanged

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
        // who cares?
    }//onAccuracyChanged

    /**
     * Logs the steps taken. Doesn't matter they're small for one men, or a giant leap for humanity.
     * Step is step.
     * @since Pedometer 1.1, march 2021
     */
    private void postProgress() {
        Log.d(Pedometer.class.getSimpleName(), "Counted to: " +steps +"/"
                +Pedometer.STEPS_TO_COUNT);
    }//postProgress

}//Pedometer SensorEventListener
