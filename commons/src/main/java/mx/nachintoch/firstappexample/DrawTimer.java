package mx.nachintoch.firstappexample;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;

/**
 * Performs the count to draw the weapon in a secondary thread.
 * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch" Castillo</a>
 * @version 2.0, march 2021
 * @since Cowboy Duel 1.0, february 2019
 */
public class DrawTimer implements Runnable {

    // class attributes

    /**
     * Indicates whether the counter has been dismissed or no (yet)-
     * @since DrawTimer 2.0, march 2021
     */
    private boolean isCancelled;

    /**
     * Reference to the ImageView which shows the weapon to draw (make visible)
     * @since DrawTimer 2.0, march 2021
     */
    private final ImageView WEAPON_IV;

    /**
     * Indicates the amount of seconds to count up to before drawing the weapon.
     * @since DrawTimer 2.0, march 2021
     */
    private final short SECONDS_TO_COUNT;

    // constructors

    /**
     * Initalizes a timer to draw (make visible) the given ImageView after the specified amount of
     * seconds.
     * @param weaponIv - The ImageView of the weapon to draw.
     * @param secondsToCount - The amunt of seconds to count up to.
     * @since DrawTimer 2.0, march 2021
     */
    public DrawTimer(ImageView weaponIv, short secondsToCount) {
        if(secondsToCount < 1) {
            throw new IllegalArgumentException("The amount of seconds to count must be positive");
        }//checks the time to count is a positive number
        WEAPON_IV = weaponIv;
        SECONDS_TO_COUNT = secondsToCount;
    }//constructor

    // methods

    /**
     * Request to cancel this counter.
     * @since DrawTimer 2.0, march 2021
     */
    public void cancel() {
        isCancelled = true;
        postVisibilityToUi(View.INVISIBLE);
    }//cancel

    @Override
    public void run() {
        short counter = 0;
        while (counter < SECONDS_TO_COUNT) {
            if(isCancelled) return;
            postProgress(counter);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                Log.d(DrawTimer.class.getSimpleName(), "The counter was interrupted");
            }// tries to sleep
            counter++;
        }//counts so many seconds
        postVisibilityToUi(View.VISIBLE);
    }//run

    /**
     * Logs the count.
     * @param count - The amount of seconds that've been count.
     * @since DrawTimer 1.0, february 2019
     */
    private void postProgress(short count) {
        Log.d(DrawTimer.class.getSimpleName(), "Counted to: " +count +"/"
                +SECONDS_TO_COUNT);
    }//postProgress

    /**
     * Asks the main thread to change the visibility of the weapon image.
     * @param visibility - Must be either View.VISIBLE or View.INVISIBLE
     * @see View#VISIBLE
     * @see View#INVISIBLE
     */
    private void postVisibilityToUi(final int visibility) {
        WEAPON_IV.post(new Runnable() {
            @Override
            public void run() {
                WEAPON_IV.setVisibility(visibility);
            }
        });
    }//postVisibilityToUi

}//DrawTimer Runnable
