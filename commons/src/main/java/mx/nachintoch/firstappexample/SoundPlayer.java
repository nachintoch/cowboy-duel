package mx.nachintoch.firstappexample;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;
import android.util.Log;

/**
 * JobIntentService to play sounds in background.
 * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch" Castillo</a>
 * @version 1.0, february 2019
 * @since Cowboy Duel 1.0, february 2019
 */
public class SoundPlayer extends JobIntentService {

    // attributes

    /**
     * Fires the sound.
     * @since Sound Player 1.0, february 2019
     */
    public static final String ACTION_FIRE = "mx.nachintoch.firstappexample.action.FIRE";

    /**
     * Identifies the Job to run in Android 8 or newer.
     * @since Sound Player 1.0, february 2019
     */
    private static final byte JOB_ID = 0;

    // methods

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        String action = intent.getAction();
        if(action == null) action = "";
        switch (action) {
            case ACTION_FIRE :
                MediaPlayer soundPlayer = MediaPlayer.create(this, R.raw.fire);
                soundPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        Log.d(SoundPlayer.class.getSimpleName(), "Freeing sound related resources");
                        mp.release();
                    }//onCompletion
                });
                soundPlayer.start();
                Log.d(SoundPlayer.class.getSimpleName(), "Bang!");
                break;
            default:
                Log.d(SoundPlayer.class.getSimpleName(), "Unrecognized action: " +action);
        }//performs the desired action
    }

    /**
     * Starts a backgound task for the SoundPlayer JobIntentService.
     * @param context - The context from which the service is being created.
     * @param work - The intent with the action and extras to start a job.
     * @since Sound Player 1.0, february 2019
     */
    public static void enqueueWork(@NonNull  Context context, @NonNull Intent work) {
        enqueueWork(context, SoundPlayer.class, JOB_ID, work);
    }//enqueueWork

}//SoundPlayer JobIntentService
