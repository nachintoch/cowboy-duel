package mx.nachintoch.firstappexample.wearable;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import android.support.wearable.activity.WearableActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import mx.nachintoch.firstappexample.DrawTimer;
import mx.nachintoch.firstappexample.Pedometer;
import mx.nachintoch.firstappexample.SoundPlayer;

/**
 * Wearable activity with the fire button and the gun image to display.
 * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch" Castillo</a>
 * @version 1.0, february 2019
 * @since Cowboy duel 1.0, february 2019
 */
public class WearableGunActivity extends WearableActivity {

    // class attributes

    /**
     * If this device supports the Step Counter Sensor, then this will hold a reference to the
     * system sensor manager. Else this will be a null pointer.
     * @since Wereable Gun Activity 1.0, february 2019
     */
    private SensorManager sensorManager;

    /**
     * Reference to the retrieved Step Sensor; if available.
     * @since Gun Activity 1.0, february 2019
     */
    @Nullable
    private Sensor stepDetectorSensor;

    /**
     * Reference to the gun view.
     * @since Gun Activity 1.0, february 2019
     */
    private ImageView gunView;

    /**
     * Reference to the application's pedometer; if available by the system sensors.
     * @since Gun Activity 1.0, february 2019
     */
    @Nullable
    private Pedometer pedometer;

    /**
     * Starts a count in a separate thread.
     * @since Gun Activity 1.0, february 2019
     */
    private DrawTimer asyncCounter;

    /**
     * Reference to the startButton
     * @since Gun Activity 1.0, february 2019
     */
    private Button startButton;

    // methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(mx.nachintoch.firstappexample.wearable.R.layout.activity_gun);
        gunView = findViewById(R.id.gun_iv);
        startButton = findViewById(R.id.start_button);
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        stepDetectorSensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);
        if(stepDetectorSensor != null) pedometer = new Pedometer(sensorManager, gunView);
    }//onCreate

    @Override
    protected void onPause() {
        killCounter();
        super.onPause();
    }//onPause

    @Override
    protected void onResume() {
        super.onResume();
        init();
    }//onResume

    /**
     * Checks if the devices has a Step Sensor. If it does, it starts the step counting. If doesnt,
     * if fallbacks to a timer.
     * @param startButton - Reference to the startButton. Its not used.
     * @since Wearable Gun Activity 1.0, february 2019
     */
    public void finalCountdown(View startButton) {
        startButton.setVisibility(View.INVISIBLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        if(stepDetectorSensor != null) {
            sensorManager.registerListener(pedometer, stepDetectorSensor,
                    SensorManager.SENSOR_DELAY_NORMAL);
            Log.d(WearableGunActivity.class.getSimpleName(), "Using pedometer");
            return;
        }//if the device has a step detector sensor
        if(asyncCounter != null && !asyncCounter.getStatus().equals(DrawTimer.Status.FINISHED))
            asyncCounter.cancel(true);
        asyncCounter = new DrawTimer();
        asyncCounter.execute(gunView);
        Log.d(WearableGunActivity.class.getSimpleName(), "Using timer");
    }//finalCountdown

    /**
     * Plays the gun shot and resets the app.
     * @param gun - Reference to the gun view.
     * @since Wearable Gun Activity 1.0, february 2019
     */
    public void fire(View gun) {
        SoundPlayer.enqueueWork(this, new Intent(SoundPlayer.ACTION_FIRE));
        gun.setVisibility(View.INVISIBLE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                init();
            }//run
        }, 3000);
    }//fire

    /**
     * Sets the application as in it initial state: shows the start button and hides the gun view.
     * @since Gun Activity 1.0, february 2019
     */
    private void init() {
        startButton.setVisibility(View.VISIBLE);
        gunView.setVisibility(View.INVISIBLE);
    }//init

    /**
     * Stops the Step count or the Time counter, depending on the availability.
     * @since Gun Activity 1.0, february 2019
     */
    private void killCounter() {
        if(sensorManager != null) sensorManager.unregisterListener(pedometer);
        else if(asyncCounter != null && !asyncCounter.getStatus().equals(AsyncTask.Status.FINISHED))
            asyncCounter.cancel(true);
    }//killCounter

}//WearableGunActivity
